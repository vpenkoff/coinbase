package apis

import (
	"github.com/vpenkoff/coinbase/apis/orders"
	"github.com/vpenkoff/coinbase/apis/products"
)

func CallProductsApi(command *string, data *string, config *string) {
	switch *command {
	case "getProductTicker":
		products.Call(data, config)
	default:
		panic("Unrecognized command request")
	}
}

func CallOrdersApi(command *string, data *string, config *string) {
	switch *command {
	case "createNewOrder":
		orders.Call(data, config)
	default:
		panic("Unrecognized command request")
	}
}
