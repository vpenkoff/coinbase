package products

import (
	"encoding/json"
	"fmt"
	"github.com/vpenkoff/coinbase/request"
)

const (
	URI = "/products/%s/ticker"
)

type productTicker struct {
	Path   string
	Method string
}

func getProductTickerApi(uri_params []interface{}) *productTicker {
	if len(uri_params) == 0 {
		fmt.Printf("Please provide product id")
	}

	var pt productTicker

	pt.Path = fmt.Sprintf(URI, uri_params...)
	pt.Method = "GET"
	return &pt
}

func GetProuctTickerResponse(response *[]byte) {

	var data interface{}

	err := json.Unmarshal(*response, &data)
	if err != nil {
		panic(err)
	}

	fmt.Println(data)
}

func Call(data *string, config *string) {
	uri_params := []interface{}{*data}
	productTicker := getProductTickerApi(uri_params)

	rc := request.CreateRequestContainer()
	rc.Path = productTicker.Path
	rc.Method = productTicker.Method
	rc.Config = *config

	resp := request.MakeRequest(&rc)
	GetProuctTickerResponse(&resp)
}
