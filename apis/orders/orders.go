package orders

import (
	"encoding/json"
	"fmt"
	"github.com/vpenkoff/coinbase/request"
)

const (
	URI = "/orders"
)

type createNewOrder struct {
	Path        string
	Method      string
	RequestBody string
}

func getCreateNewOrderApi(request_body string) *createNewOrder {
	var newOrder createNewOrder

	newOrder.Path = URI
	newOrder.Method = "POST"
	newOrder.RequestBody = request_body

	return &newOrder
}

func GetCreateNewOrderResponse(response *[]byte) {

	var data interface{}

	err := json.Unmarshal(*response, &data)
	if err != nil {
		panic(err)
	}

	fmt.Println(data)
}
func Call(data *string, config *string) {
	createNewOrder := getCreateNewOrderApi(*data)
	rc := request.CreateRequestContainer()
	rc.Config = *config
	rc.Path = createNewOrder.Path
	rc.Method = createNewOrder.Method
	rc.Body = createNewOrder.RequestBody
	resp := request.MakeRequest(&rc)
	GetCreateNewOrderResponse(&resp)
}
