package cfg

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
)

type coinBaseConfig struct {
	Key        string
	Secret     string
	Passphrase string
	Host       string
}

func GetConfig(configPath *string) coinBaseConfig {
	file, err := os.Open(*configPath)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	conf := coinBaseConfig{}

	err = decoder.Decode(&conf)
	if err != nil {
		fmt.Println("Error reading config file:", err)
	}

	return conf
}
