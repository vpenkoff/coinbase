package request

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"github.com/vpenkoff/coinbase/cfg"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func createSignature(request_path string, body string, timestamp string, method string, secret string) string {

	payload := fmt.Sprintf("%s%s%s%s", timestamp, method, request_path, body)

	decoded_secret, err := base64.StdEncoding.DecodeString(secret)
	if err != nil {
		fmt.Println("Base64 Decoding failed: ", err)
	}

	signature := hmac.New(sha256.New, decoded_secret)
	signature.Write([]byte(payload))

	encodedSignature := base64.StdEncoding.EncodeToString(signature.Sum(nil))

	return encodedSignature
}

func PrepareRequest(req *requestContainer) (*http.Request, interface{}) {
	var uri string

	config := cfg.GetConfig(&req.Config)

	req.Host = config.Host

	if req.Port != "" {
		uri = fmt.Sprintf("%s:%s%s", req.Host, req.Port, req.Path)
	} else {
		uri = fmt.Sprintf("%s%s", req.Host, req.Path)
	}

	timeNow := strconv.FormatInt(time.Now().Unix(), 10)

	payload := createSignature(uri, req.Body, timeNow, req.Method, config.Secret)

	request, err := http.NewRequest(req.Method, uri, strings.NewReader(req.Body))

	req.Headers = map[string]string{"Content-Type": "application/json",
		"CB-ACCESS-KEY":        config.Key,
		"CB-ACCESS-SIGN":       payload,
		"CB-ACCESS-TIMESTAMP":  timeNow,
		"CB-ACCESS-PASSPHRASE": config.Passphrase}

	if len(req.Headers) != 0 {
		for k, v := range req.Headers {
			request.Header.Add(k, v)
		}
	}

	return request, err
}
