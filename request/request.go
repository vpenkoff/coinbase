package request

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

type requestContainer struct {
	Host    string
	Port    string
	Path    string
	Headers map[string]string
	Body    string
	Method  string
	Config  string
}

func CreateRequestContainer() requestContainer {
	var rc requestContainer
	return rc
}

func createClient() *http.Client {
	client := &http.Client{}
	return client
}

func MakeRequest(req *requestContainer) []byte {
	client := createClient()
	request, err := PrepareRequest(req)

	resp, err := client.Do(request)
	if err != nil {
		fmt.Println(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		fmt.Println(err)
	}

	return body
}
