# Coinbase Library
### Plugable library for requests to [coinbase](https://www.coinbase.com/).
## How to use
Include the library in you project, i.e.:

`import "github.com/vpenkoff/coinbase"`

and call it:

`coinbase.CallApi(api *string, command *string, data *string, config *string)` or

`coinbase.CallApi("products", "getProductTicker", "BTC-USD", "/home/joe/configs/coinbase_cfg.json")`

Example configuration file.json:
```
{
        "host": "https://api.pro.coinbase.com",
        "key": "imatestkey",
        "secret": "immasecret",
        "passphrase": "h4x0r43v3r"
}
```
### Checkout also [coinbase-cli](https://github.com/vpenkoff/coinbase-cli)  for cli interface to the library.

***

##Todo:
1. Tests
2. Better description
