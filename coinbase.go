package coinbase

import (
	"github.com/vpenkoff/coinbase/apis"
)

func CallApi(api *string, command *string, data *string, config *string) {
	switch *api {
	case "products":
		apis.CallProductsApi(command, data, config)
	case "orders":
		apis.CallOrdersApi(command, data, config)
	default:
		panic("Unrecognized api request")
	}
}
